---
title: "oct-unfold"
#series: "Manual"
---


### NAME
oct-unfold - Band-structure unfolding

### DESCRIPTION
This program is one of the {{< octopus >}} utilities.

Its behavior is explained in the [Unfolding tutorial](../../../tutorial/periodic_systems/unfolding). 

---------------------------------------------
