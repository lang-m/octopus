---
title: "Installation"
series: "Manual"
weight: 4
---


Maybe somebody else installed {{< octopus >}} for you. In that case, the files
should be under some directory that we can call {{< inst_file >}}, the executables
in {{< inst_file "bin/" >}} (e.g. if {{< inst_file >}}={{< file "/usr/local/" >}}, the main {{< octopus >}}
executable is then in {{< file "/usr/local/bin/octopus" >}}); the pseudopotential files that {{< octopus >}} will
need in {{< inst_file "share/octopus/PP/" >}}, etc.

However, you may be unlucky and that is not the case. In the following we will try
to help you with the still rather unfriendly task of compiling and installing the {{< octopus >}}.


## Downloading

Download the latest {{< octopus >}} version here: {{< octopus-download >}}.



{{%notice note%}}
Make sure you download the correct tar-ball, depending on whether you want to use CMake (recommended) or Autotools.
{{%/notice%}}

## Building

### Quick instructions

For the impatient, here is the quick-start:

```shell
$ tar xzf octopus-{{< octopus-version >}}.tar.gz
$ cd octopus-{{< octopus-version >}}
$ cmake -B ./build -G Ninja --install-prefix={{<command "<INSTALLATION_DIR>">}}
$ cmake --build ./build
$ ctest --test-dir ./build -L short-run
$ cmake --install ./build
```


With CMake it is now possible to use any version directly from the Gitlab repository.

```shell
$ git clone https://gitlab.com/octopus-code/octopus
$ cd octopus
$ cmake -B ./build -G Ninja
...
```

This will probably {{< emph "not" >}} work, so before giving up, just read
the following more detailled pages.


For more details on building Octopus using CMake see the {{<manual "installation/CMake" "CMake readme">}}.

In case you still want to use Autotools, you can find more information 
{{<manual "installation/Autotools" "Autotools readme">}}.

---------------------------------------------
