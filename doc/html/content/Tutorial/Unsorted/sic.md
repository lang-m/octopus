---
title: "Self-interaction correction schemes"
section: "Tutrials"
author: "Nicolas Tancogne-Dejean"
theories: "DFT"
features: "SIC"
difficulties: "advanced"
calculation_modes: "Ground-state"
system_types: ["Atom"]
species_types: "Pseudopotentials"
---

It is well known that local and semilocal functionals suffer from the so-called self-interaction error.
One possible way to improve upon this is to correct the DFT energy using some self-interaction correction (SIC) schemes, as proposed originally by Perdew and Zunger.[^footnote-1]

In Octopus, we provide several levels of SIC , including the average-density SIC (ADSIC), the Amaldi SIC, and the Perdez-Zunger SIC. These schemes are available for unpolarized and spin-polarized calculations. For spinors, only ADSIC and Perdez-Zunger SIC are available, following the noncollinear extension of these schemes proposed in Ref. [^footnote-2].

###  Average-density SIC

 In this tutorial, we will see how to employ such a scheme, focusing on the ADSIC and PZ-SIC applied to a single atom.
Here is the minimal input file needed to perform the calculation for ADSIC
{{< code-block >}}
#include_input doc/tutorials/other/sic/inp
{{< /code-block >}}

The only input option that is used here that relates to the SIC correction is the variable {{< variable "SICCorrection" >}}. It is set to use here the ADSIC correction scheme.
Running this example produces the following eigenvalues, written in the <tt>static/info</tt> file.
{{< code-block >}}
#include_input doc/tutorials/other/sic/eigenvalues.txt
{{< /code-block >}}

The highest occupied eigenvalue (here three times degenerate) leads to an ionization energy of 15.98 eV.
Redoing the same calculation without SIC produces an ionization energy of 10.38 eV. 
The experimental value lies between 15.7 to 15.8  eV (see the <a href="https://webbook.nist.gov/cgi/inchi?ID=C7440371&Mask=20"> NIST</a> website), showing that the ADSIC can improve considerably the ionization energy obtained from local and semi-local functionals like LDA.

###  Perdew-Zunger SIC

If one wants to use Perdew-Zunger SIC, instead of ADSIC, one simply needs to change {{< variable "SICCorrection" >}} to <tt>sic_pz</tt>. However, it is important to note that the Perdew-Zunger SIC requires the solution of the OEP equations. By default, Octopus uses the KLI approximation to the full solution. The level of approximation for the OEP equation is controled by the variable {{< variable "OEPLevel" >}}.
Finally, it is important to note that the Perdew-Zunger SIC depends on the individual Kohn-Sham orbitals. A unitary transformation of these orbitals therefore changes the result of the Perdew-Zunger SIC. 
Octopus implements the Wannierization scheme proposed by Lin and co-workers, the SCDM method [^footnote-3], for transforming the Kohn-Sham to more localized orbitals. This can be done using the variable {{< variable "SCDMforPZSIC">}}.



{{<tutorial-footer>}}

##  References
<references/>

[^footnote-1]: {{< article title="Self-interaction correction to density-functional approximations for many-electron systems" authors="J. P. Perdew and A. Zunger" journal="Phys. Rev. B" year="1981" volume="23" pages="5048" doi="10.1103/PhysRevB.23.5048" >}}
[^footnote-2]: {{< article title="Self-interaction correction schemes for non-collinear spin-density-functional theory" authors="N. Tancogne-Dejean, M. Lueders, C. A. Ullrich" journal="J. Chem. Phys." year="2023" volume="159" pages="224110" doi="10.1063/5.0179087" >}}
[^footnote-3]: {{< article title="Compressed Representation of Kohn–Sham Orbitals via Selected Columns of the Density Matrix" authors="A. Damle, L. Lin, L. Ying" journal="J. Chem. Theory Comput." year="2015" volume="11" pages="1463" doi="10.1021/ct500985f" >}}


