---
title: "Sternheimer linear response"
#tags: ["Tutorial", "Advanced", "Electromagnetic Response", "Molecule", "Pseudopotentials", "DFT", "Optical Absorption", "Sternheimer"]
difficulties: "advanced"
theories: "DFT"
calculation_modes: "Electromagnetic Response"
system_stypes: "Molecule"
species_types: "Pseudopotentials"
features: ["Optical Absorption", "Sternheimer"]
series: "Tutorials"
description: "More details on the Sternheimer approach."
---


The Sternheimer approach to perturbation theory allows efficient calculations of linear and non-linear response properties.
[^footnote-1]

The basis of this method, just as in standard perturbation theory, is to calculate the variation of the wave-functions $\psi^{1}$ under a given perturbing potential. The advantage of the method is that the variations are obtained by solving the linear equation

$$
(H^0-\\epsilon^0 + \\omega )|\\psi^{1}\>=-P\_{\\rm c} H^{1}|\\psi^{0}\>\\ ,
$$

that only depends on the occupied states instead of requiring an (infinite) sum over unoccupied states. In the case of (time-dependent) density functional theory the variation of the Hamiltonian includes a term that depends on the variation of the density, so this equation must be solved self-consistently.

To run a Sternheimer calculation with {{< octopus >}}, the only previous calculation you need is a ground-state calculation.
For this tutorial we will use a water molecule, with this basic input file for the ground state:

{{< code-block >}}
#include_input doc/tutorials/other/sternheimer_lr/inp_gs

 {{< variable "CalculationMode" >}} = gs

 %{{< variable "Coordinates" >}}
  'O'  |  0.000000  | -0.553586  |  0.000000
  'H'  |  1.429937  |  0.553586  |  0.000000
  'H'  | -1.429937  |  0.553586  |  0.000000
 %

 {{< variable "Radius" >}} = 10
 {{< variable "Spacing" >}} = 0.435
 {{< variable "ConvRelDens" >}} = 1e-6
{{< /code-block >}}

We use a tighter setting on SCF convergence (ConvRelDens) which will help the ability of the Sternheimer calculation to converge numerically, and we increase a bit the size of the box as response calculations tend to require more space around the molecule than ground-state calculations to be converged.
[^footnote-2]


After the ground-state calculation is finished, we change the run mode to {{< code "em_resp" >}}, to run a calculation of the electric-dipole response:

{{< code-block >}}
 {{< variable "CalculationMode" >}} = em_resp
{{< /code-block >}}

Next, to specify the frequency of the response we use the {{< variable "EMFreqs" >}} block; in this case we will use three values 0.00, 0.15 and 0.30 {{< units "{{< Hartree >" >}}}}:

{{< code-block >}}
 %{{< variable "EMFreqs" >}}
 3 | 0.0 | 0.3
 %
{{< /code-block >}}

and we will also specify a small imaginary part to the frequency of 0.1 {{< units "eV" >}}, which avoids divergence on resonance:

{{< code-block >}}
 {{< variable "EMEta" >}} = 0.1*eV
{{< /code-block >}}

and finally we add a specification of the linear solver, which will greatly speed things up compared to the default:

{{< code-block >}}
 {{< variable "LinearSolver" >}} = qmr_dotp
 {{< variable "ExperimentalFeatures" >}} = yes
{{< /code-block >}}

In the run, you will see calculations for each frequency for the ''x'', ''y'', and ''z'' directions, showing SCF iterations, each having linear-solver iterations for the individual states' $\psi^{1}$, labelled by the k-point/spin (ik) and state (ist). The norm of $\psi^{1}$, the number of linear-solver iterations (iter), and the residual $\left|(H^0-\epsilon^0 + \omega )|\psi^{1}>+P_{\rm c} H^{1}|\psi^{0}>\right|$ are shown for each. First we see the static response:

```
#include_file doc/tutorials/other/sternheimer_lr/Response.txt
```

Later will come the dynamical response. The negative state indices listed indicate response for $-\omega$. For each frequency, the code will try to use a saved response density from the closest previously calculated frequency.

```
#include_file doc/tutorials/other/sternheimer_lr/Response-x.txt
```


At the end, you will have a directory called {{< file "em_resp" >}} containing a subdirectory for each frequency calculated, each in turn containing {{< file "eta" >}} (listing $\eta$ = 0.1 {{< units "eV" >}}), {{< file "alpha" >}} (containing the real part of the polarizability tensor), and {{< file "cross_section" >}} (containing the cross-section for absorption, based on the imaginary part of the polarizability).

For example, {{< file "em_resp/freq_0.0000/alpha" >}} says

```
#include_file doc/tutorials/other/sternheimer_lr/alpha
```

Exercise: compare results for polarizability or cross-section to a calculation from time-propagation or the Casida approach.










---------------------------------------------
[^footnote-1]: {{< article title="Time-dependent density functional theory scheme for efficient calculations of dynamic (hyper)polarizabilities" authors="Xavier Andrade, Silvana Botti, Miguel Marques and Angel Rubio" journal="J. Chem. Phys" volume="126" pages="184106" year="2007" doi="10.1063/1.2733666" >}}

[^footnote-2]: {{< article title="Basis set effects on the hyperpolarizability of CHCl<sub>3</sub>: Gaussian-type orbitals, numerical basis sets and real-space grids" authors="F. D. Vila, D. A. Strubbe, Y. Takimoto, X. Andrade, A. Rubio, S. G. Louie, and J. J. Rehr" journal="J. Chem. Phys." volume="133" pages="034111" year="2010" doi="10.1063/1.3457362" >}}
