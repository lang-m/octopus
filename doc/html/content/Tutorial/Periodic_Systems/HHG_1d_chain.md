---
title: "High-harmonic generation in solids"
series: "Tutorials"
tutorials: ["Periodic Systems"]
difficulties: "Beginner"
system_types: "Bulk"
calculation_modes: "Time-dependent"
description: "How to obtain the high-harmonic spectrum of a periodic 1D chain of atoms."
weight: 6
---


In this tutorial we will explore how to compute high-harmonic generation (HHG) of bulk systems from real-time TDDFT using {{< octopus >}}.
###  Ground state calculation

As always, we will start with the input file for the ground state. In this tutorial, we will consider a 1D periodic chain of hydrogen dimers, for simplicity. 

{{<code-block >}}
#include_input doc/tutorials/periodic_systems/hhg_1d_chain/1.gs/inp
{{</code-block >}}


###  Time-dependent calculation

After getting the ground state of the system, we now look into computing the HHG.
In order to compute the HHG, we need to perform a real-time simulation for which we evaluate the electronic current of the system.
From it, the HHG is obtained as the power spectrum of its time derivative, i.e.,
$$
{\rm HHG}(\omega) = \left|{\rm FT}\left(\int d\mathbf{r}\partial_t \mathbf{j}(\mathbf{r},t)\right)\right|^2
$$ 

In order to perform the simulation, we need to define the laser parameters. We will use here a photon frequency corresponding to $\hbar \omega = 0.75$ eV, a full duration of 4 optical cycles, with a sin-square envelope. The intensity is taken as $I=10^{10}$W.cm$^{-2}$.

The corresponding input file is given below:
{{<code-block >}}
#include_input doc/tutorials/periodic_systems/hhg_1d_chain/2.td/inp
{{</code-block >}}

After running the simulation, one obtains the total electronic current that looks like

#include_eps doc/tutorials/periodic_systems/hhg_1d_chain/2.td/HHG_1d_chain_current.eps caption="Fig. 1. Total electronic current of the periodic one-dimensional hydrogen dimer chain. The dotted line shows the time profile of the external vector potential."

From this plot, it is clear that the current responds mostly linearly to the external laser field (shown by the dotted line). The small superimposed distortions and subsequent oscillations correspond to the nonlinear response to the laser field, which we are trying to resolve. 
In order to see this better, we now look to the harmonic spectrum of the laser field.

### High-harmonic spectrum

From this electronic current, one directly obtains the HHG, employing the utility "oct-harmonic_spectrum". This produces a file called "hs-mult.x", which, once plotted, is giving the following spectrum

#include_eps doc/tutorials/periodic_systems/hhg_1d_chain/3.spectrum/HHG_1d_chain_hhg.eps caption="Fig. 2. High-harmonic spectrum of the periodic one-dimensional hydrogen dimer chain."

As expected, we observe that the power spectrum of the current contains higher-order contributions, i.e. high-order harmonics are generated thanks to nonlinear effects. 
However, it is important to note that the present calculation is not converged (neither in box size in the non-periodic directions, nor in grid spacing or k-point grid) . In practical calculation, one needs to be careful with the convergence, especially with respect to the number of k-points, as the aims is to resolve tiny oscillations of the current that can be several orders of magnitude smaller than the linear response.
The green curve in Fig. 2, shows the result of the same calculation but performed with 50 k-points instead of 25 k-points as done here.
