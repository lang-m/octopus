Info: Parallelization in Ions
Info: Node in group    0 will manage      6 Ions:     1 -      6
Info: Node in group    1 will manage      6 Ions:     7 -     12
Info: Node in group    2 will manage      6 Ions:    13 -     18
Info: Node in group    3 will manage      5 Ions:    19 -     23
Info: Could not find 'restart/partition' directory for restart.
Info: No restart information will be read.
Info: Using METIS 5 multilevel recursive bisection to partition the mesh.
Info: Mesh Partition restart information will be written to 'restart/partition'.
Info: Finished writing information to 'restart/partition'.
Info: Mesh partition:

      Partition quality:    0.151969E-08

                 Neighbours         Ghost points
      Average  :          3                12754
      Minimum  :          3                10605
      Maximum  :          3                14388

      Nodes in domain-group      1
        Neighbours     :         3        Local points    :     35082
        Ghost points   :     12968        Boundary points :     13622
      Nodes in domain-group      2
        Neighbours     :         3        Local points    :     35082
        Ghost points   :     10605        Boundary points :     15096
      Nodes in domain-group      3
        Neighbours     :         3        Local points    :     35082
        Ghost points   :     14388        Boundary points :     14443
      Nodes in domain-group      4
        Neighbours     :         3        Local points    :     35083
        Ghost points   :     13053        Boundary points :     14976

