#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/


cp inp_gs inp
mpirun -n 4 octopus |tee log

grep Fermi static/info > fermi.txt

cp inp_wannier inp
mpirun -n 4 octopus |tee log

oct-wannier90

cp w90.win_mod w90.win

wannier90.x -pp w90.win

cp inp_wannier_out inp

oct-wannier90

wannier90.x w90.win

fermi=`awk '{print $4*27.2114}' fermi.txt`

cat << EOF >> w90.win
restart = plot
fermi_energy = $fermi
fermi_surface_plot = true
EOF


wannier90.x w90.win

cp inp_gs inp_wannier w90.win tutorial.sh fermi.txt  $OCTOPUS_TOP/doc/tutorials/other/wannier90
