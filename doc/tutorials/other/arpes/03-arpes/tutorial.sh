#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt

oct-photoelectron_spectrum |tee log

gnuplot plot.gnu

cp tutorial.sh *.txt *.eps $OCTOPUS_TOP/doc/tutorials/other/arpes/03-arpes/
