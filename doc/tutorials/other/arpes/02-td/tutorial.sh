#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt

octopus |tee log

$HELPER_DIR/extract.sh log 'Photoelectron' > Photoelectron.txt


cp tutorial.sh *.txt $OCTOPUS_TOP/doc/tutorials/other/arpes/02-td/
