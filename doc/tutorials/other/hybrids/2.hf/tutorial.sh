#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps

mpirun -n 4 octopus > log

$HELPER_DIR/extract.sh log "Theory Level" > Theory_level.txt

grep -A 2 "Direct gap at"  static/info > gap.txt

cp tutorial.sh *.txt inp  $OCTOPUS_TOP/doc/tutorials/other/hybrids/2.hf/
