#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps
mpirun -n 4 octopus |tee log

grep -B 50 -A 6 'MINIMIZATION ITER #:    1' log > tmp.out
$HELPER_DIR/extract_generic.sh tmp.out 'Info: Start' > Iteration_1.txt

grep -B 10 -A 2 'final coordinates' log > Iteration_last.txt

rm tmp.out

cp tutorial.sh  plot.gp *.txt *.eps min.xyz  $OCTOPUS_TOP/doc/tutorials/other/geometry_optimization/2.sodium_trimer/1.cg_bfgs
