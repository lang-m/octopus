set xlabel "Energy (1/cm)"
set ylabel "Spectrum (a.u.)"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "H2_diss.eps"

unset key

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5

set xlabel "d [a.u.]"
set ylabel "E [a.u.]"

plot "DIST_series.dat" u 1:2 w l lw 3
