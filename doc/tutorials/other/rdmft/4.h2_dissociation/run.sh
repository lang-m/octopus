#!/bin/bash

series=DIST-series
outfile="$series.dat"
echo "#Dist    Energy    1. NON  2.NON" > $outfile
list="0.25 0.5 0.75 1.0 1.5 2.0 2.5 3.0 4.0 5.0 6.0 8.0"


export OCT_PARSE_ENV=1

for param in $list
do
  echo $param
  folder="$series-$param"
  out_tmp=out-RDMFT-$param

  sed "{s/DISTANCE/$param/g}" inp_template > inp  
  rm -rf restart

  export OCT_THEORYLEVEL="independent_particles"
  mpirun -n 8 octopus
  export OCT_THEORYLEVEL="rdmft"
  mpirun -n 8 octopus |tee ${out_tmp}

  energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
  seigen=`grep -a " 1  " static/info |  awk '{print $2}'`
  peigen=`grep -a " 2  " static/info |  awk '{print $2}'`
  echo $param $energy $seigen $peigen >> $outfile
done
