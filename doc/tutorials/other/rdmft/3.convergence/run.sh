#!/bin/bash

series=ES-series
outfile="$series.dat"
echo "#ES    Energy    1. NON  2.NON" > $outfile
list="4 9 14 19 24 29"

export OCT_PARSE_ENV=1
for param in $list
do
  folder="$series-$param"
  out_tmp=out-RDMFT-$param
  
  export OCT_EXTRASTATES=${param}

  export OCT_THEORYLEVEL="independent_particles"
  mpirun -n 4 octopus 

  export OCT_THEORYLEVEL="rdmft"
  mpirun -n 4 octopus |tee ${out_tmp}

  energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
  seigen=`grep -a " 1  " static/info |  awk '{print $2}'`
  peigen=`grep -a " 2  " static/info |  awk '{print $2}'`
  echo $param $energy $seigen $peigen >> $outfile
done
