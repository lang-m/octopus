set xlabel "Energy (1/cm)"
set ylabel "Spectrum (a.u.)"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "Total_Energy.eps"

unset key

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5

set xlabel "Number of extra states"
set ylabel "E [a.u.]"

plot "ES-series.dat" u 1:2 w lp lw 3
