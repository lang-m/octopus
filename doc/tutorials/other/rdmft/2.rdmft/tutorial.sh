#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps

mpirun -n 4 octopus |tee log

$HELPER_DIR/extract.sh log "Theory Level" > Theory_level.txt
$HELPER_DIR/extract_generic static/info "Natural " > Occupation.txt 


cp tutorial.sh *.txt inp  $OCTOPUS_TOP/doc/tutorials/other/rdmft/2.rdmft/
