# nspin         2
# kick mode    1
# kick strength    0.005291772086
# dim           3
# pol(1)           1.000000000000    0.000000000000    0.000000000000
# pol(2)           0.000000000000    1.000000000000    0.000000000000
# pol(3)           0.000000000000    0.000000000000    1.000000000000
# direction    1
# Equiv. axes  0
# wprime           0.000000000000    0.000000000000    1.000000000000
# kick time        0.000000000000
#%
# Number of time steps =     2500
# PropagationSpectrumDampMode   =    2
# PropagationSpectrumDampFactor =   -27.2114
# PropagationSpectrumStartTime  =     0.0000
# PropagationSpectrumEndTime    =    10.0000
# PropagationSpectrumMinEnergy  =     0.0000
# PropagationSpectrumMaxEnergy  =    20.0000
# PropagationSpectrumEnergyStep =     0.0100
#%
#       Energy        sigma(1, nspin=1)   sigma(2, nspin=1)   sigma(3, nspin=1)   sigma(1, nspin=2)   sigma(2, nspin=2)   sigma(3, nspin=2)   StrengthFunction(1) StrengthFunction(2)
#        [eV]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]               [1/eV]              [1/eV]       
      0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00     -0.00000000E+00
      0.10000000E-01     -0.40531697E-08      0.28358569E-16     -0.23602673E-17      0.40455575E-08     -0.20156757E-16      0.99938014E-17     -0.36927239E-08      0.36857886E-08
      0.20000000E-01     -0.16155196E-07      0.11329552E-15     -0.94306890E-17      0.16124716E-07     -0.80526568E-16      0.39923669E-16     -0.14718524E-07      0.14690755E-07
      0.30000000E-01     -0.36134439E-07      0.25439538E-15     -0.21180195E-16      0.36065740E-07     -0.18080865E-15      0.89635318E-16     -0.32921025E-07      0.32858436E-07
