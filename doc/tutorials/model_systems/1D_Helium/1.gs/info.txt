**************************** Theory Level ****************************
Input: [TheoryLevel = independent_particles]
**********************************************************************

SCF converged in   19 iterations

Some of the states are not fully converged!
Eigenvalues [H]
 #st  Spin   Eigenvalue      Occupation
   1   --    -2.238257       1.000000

Energy [H]:
      Total       =        -2.23825730
      Free        =        -2.23825730
      -----------
      Ion-ion     =         0.00000000
      Eigenvalues =        -2.23825730
      Hartree     =         0.00000000
      Int[n*v_xc] =         0.00000000
      Exchange    =         0.00000000
      Correlation =         0.00000000
      vanderWaals =         0.00000000
      Delta XC    =         0.00000000
      Entropy     =         1.38629436
      -TS         =        -0.00000000
      Photon ex.  =         0.00000000
      Kinetic     =         0.28972530
      External    =        -2.52798260
      Non-local   =         0.00000000
      Int[n*v_E]  =         0.00000000

Dipole:                 [b]          [Debye]
      <x> =    6.36804E-08      1.61860E-07
      <y> =   -6.45436E-08     -1.64053E-07

Convergence:
      abs_energy =  6.49258425E-13 ( 0.00000000E+00) [H]
      rel_energy =  2.90073186E-13 ( 0.00000000E+00)
      abs_dens =  3.65703349E-07 ( 0.00000000E+00)
      rel_dens =  3.65703349E-07 ( 1.00000000E-06)
      abs_evsum =  6.49258425E-13 ( 0.00000000E+00) [H]
      rel_evsum =  2.90073186E-13 ( 0.00000000E+00)

