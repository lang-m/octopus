set xlabel "Time [a.u.]"
set ylabel "Total current [a.u.]"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "HHG_1d_chain_current.eps"
unset key

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5

plot 'td.general/total_current' u 2:3 w l lw 3 t 'J_x(t)',\
     'td.general/laser' u 2:($3/3000) w l dt 3 t 'A_(t)', 
