#!/usr/bin/python3

import re
import sys

assert len(sys.argv) == 3, 'usage: ./autotools_check_config_h.py "path/to/config.h" "path/to/config.log"'


SPECIAL_LIBRARIES = [
    'GSL',  # does not appear in config.h
]

missing_library = False
_, config_h_path, config_log_path = sys.argv


with open(config_h_path) as f:
    config_h = f.read()


with open(config_log_path) as f:
    for line in f:
        if not re.match(r"^\s+\$(.*?)configure", line):
            continue
        configure_flags = line
        break


for lib_match in re.finditer(r'with-(?P<lib>.*?)=', configure_flags):
    lib_name = lib_match.group('lib').replace('-prefix', '').upper().replace('-', '_')
    if lib_name not in SPECIAL_LIBRARIES and f"#define HAVE_{lib_name}" not in config_h:
        missing_library = True
        print(f"Could not find '{lib_name}' in config.h")


for option_request in re.finditer(r'--enable-(?P<lib>[^ ]+)', configure_flags):
    option_name = option_request.group('lib').upper().replace('-', '_')
    if f"#define HAVE_{lib_name}" not in config_h:
        missing_library = True
        print(f"Could not find '{lib_name}' in config.h")

sys.exit(missing_library)
