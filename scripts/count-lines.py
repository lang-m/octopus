# Copyright (c) 2024 A Buccheri

"""Count total lines of files with specified extensions, as a function of git commit history.

This script iterates through a range of Git commits on a specified branch, checking out each commit
in a temporary workspace using git workdir. For each commit, it counts the total lines of code for files with
specified extensions within the repository. Git submodules are not automatically checked out, and therefore
do not need to be explicitly omitted from the search directories.

Development Notes:
 - Use of `git diff --numstat {current_commit} {prior_commit}`
   to track the net change in line count of a given file type, between adjacent
   commits, fails because total lines does not need to be conserved (established empirically).
- Use of `git s-tree -r` is not appropriate as it only returns files
  touched for a given commit, not all files present in the repo for
  that commit.
- The original script used  'git rev-list -n 1 --before="{0}-{1}-01 00:00" main'.format(year, '{0:02d}'.format(month))
  to sample the git history at monthly intervals. One could remove alpha in favour of this approach
"""

from __future__ import annotations

import datetime
import shutil
from pathlib import Path
import subprocess
import tempfile
from typing import Optional, List, Tuple

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np


def total_lines(ext: str, directory: Optional[str] = None) -> int:
    # Specific to Octopus
    blacklist_dirs = [".git", "external_libs"]
    exclude_str = " ".join(f"-path './{ignore}' -prune -o" for ignore in blacklist_dirs)
    command = f"find . {exclude_str} -iname '*.{ext}' -print0 | xargs -0 cat | wc -l"

    # shell=True handles pipes in `command`
    result = subprocess.run(
        command, shell=True, cwd=directory, text=True, check=True, capture_output=True
    )

    if result.stderr:
        print(f"Command {command} has raised the error {result.stderr}")
        raise subprocess.SubprocessError(result.returncode, command)

    line_count_str = result.stdout
    return int(line_count_str)


def checkout_git_worktree(directory: str | Path):
    # NB, one must run this command within a git repository
    print(f"Creating work directory {directory}")

    worktree = ["git", "worktree", "add", "-f", "--detach", directory]
    subprocess.run(worktree, check=True, capture_output=True)

    # git worktree should create a directory if it does not exist
    if not Path(directory).is_dir():
        raise NotADirectoryError(
            f"The expected git worktree directory, {directory}, is not present"
        )


def get_git_commit_date(commit: str, cwd: str | Path) -> datetime.datetime:
    result = subprocess.run(
        ["git", "show", "-s", "--format=%cd", "--date=short", commit],
        check=True,
        encoding="utf-8",
        capture_output=True,
        cwd=cwd,
    )
    commit_date = result.stdout.strip()
    date = datetime.datetime.strptime(commit_date, "%Y-%m-%d")
    return date


def get_git_commits(
    branch, start_date: datetime.datetime, end_date: datetime.datetime
) -> List[str]:
    start_date_str = start_date.strftime("%Y-%m-%d")
    end_date_str = end_date.strftime("%Y-%m-%d")
    command = [
        "git",
        "rev-list",
        "--reverse",
        f'--before="{end_date_str}T00:00:00Z"',
        f'--after="{start_date_str}T00:00:00Z',
        branch,
    ]
    result = subprocess.run(command, check=True, capture_output=True, encoding="utf-8")
    commits = result.stdout.split("\n")
    return commits


def line_count_history(commits: List[str], extensions: List[str]) -> Tuple[list, dict]:
    # Checkout git worktree
    work_dir = f"{tempfile.mkdtemp()}/oct_tmp_count_files"

    if Path(work_dir).is_dir():
        cmd = ["git", "worktree", "remove", work_dir]
        subprocess.run(cmd, check=True)

    checkout_git_worktree(work_dir)

    # Extract total line counts per commit, for specified extensions
    line_length = {ext: np.empty(shape=len(commits)) for ext in extensions}
    dates = []

    for i, commit in enumerate(commits):
        subprocess.run(
            ["git", "checkout", "--force", commit],
            cwd=work_dir,
            capture_output=True,
            check=True,
        )
        dates.append(get_git_commit_date(commit, work_dir))
        for ext in extensions:
            n_lines = total_lines(ext, directory=work_dir)
            line_length[ext][i] = n_lines

    shutil.rmtree(work_dir)

    return dates, line_length


if __name__ == "__main__":
    # Percentage of commits to perform line-counting on
    alpha = 0.01
    # inside the CI no local branches are created; therefore we have to use origin/main
    branch = "origin/main"
    # Case-insensitive
    extensions = ["f90", "c", "h", "cl", "cc"]

    print(
        "Computing line account as a function of commit history\n"
        "Warning: This can take several minutes when requesting the full history"
    )
    # Git history range
    start_date = datetime.datetime(2002, 1, 25)
    commits = get_git_commits(branch, start_date, datetime.datetime.now())
    assert (
        commits[0] == "aa23c8bfa38b64e48feeef199ea173f80c25f6d7"
    ), "First relevant commit"

    # Sample the commit history to significantly speed up the calculation
    # This assumes that the date between individual commits << (end_date - start_date)
    if alpha != 1.0:
        print(f"Sampling {100 * alpha}% of the commits")
        stride = round(1 / alpha)
        commits = commits[::stride]

    dates, line_length = line_count_history(commits, extensions)

    # Sanity check
    count_str = " ".join(str(line_length[ext][0]) for ext in extensions)
    first_line = f'{commits[0]} {dates[0].strftime("%Y-%m-%d")} {count_str}'
    expected_first_line = (
        "aa23c8bfa38b64e48feeef199ea173f80c25f6d7 2002-01-25 8255.0 0.0 14.0 0.0 0.0"
    )
    assert (
        first_line == expected_first_line
    ), "First line of file is not consistent with reference"

    # Dump results: Git hash, date, f90, c, h, cl, cc
    with open(file="lines.dat", mode="w") as fid:
        for i, commit in enumerate(commits):
            count_str = " ".join(str(line_length[ext][i]) for ext in extensions)
            fid.write(f'{commit} {dates[i].strftime("%Y-%m-%d")} {count_str}\n')

    # Plot result
    fig, ax = plt.subplots()
    mdates.set_epoch(start_date.strftime("%Y-%m-%d"))
    x_points = mdates.date2num(dates)

    for ext in extensions:
        ax.plot(x_points, line_length[ext], label=f"{ext}")

    # Set the desired number of ticks
    locator = mdates.AutoDateLocator(minticks=5, maxticks=24)
    formatter = mdates.DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    plt.xlabel("Date", fontsize=12)
    plt.ylabel(r"Number of Lines ($10^3$)", fontsize=12)
    plt.xticks(rotation=45, fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(fontsize=12)
    plt.tight_layout()
    # plt.show()
    plt.savefig("oct_lines.png", dpi=300, bbox_inches="tight")
