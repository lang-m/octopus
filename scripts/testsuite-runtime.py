#!/usr/bin/env python3

import argparse
import os.path
import sys
import textwrap

from junitparser import JUnitXml

def pretty_print_tests(test_cases):
    """
    Print names and runtime of the provided test_cases in a two-column formatting.
    """
    names = [test_case.name for test_case in test_cases]
    times = [test_case.time for test_case in test_cases]
    name_column_width = max(len(name) for name in names)
    print(f"{len(test_cases)} longest tests:")
    print(f"{'name':{name_column_width}} time (s)")
    print("-" * (name_column_width + 9))
    for name, time in zip(names, times):
        print(f"{name:{name_column_width}}     {time:4.0f}")
    print()


def main(filename, max_runtime=None):
    """
    Read JUnitXml from filename to extract data about testsuite runtime.

    If max_runtime is provided the program exits with non-zero return code if any of
    the testcases exceeds max_runtime.
    """
    assert os.path.exists(filename)

    xml = JUnitXml.fromfile(filename)

    executed_test_cases = sorted(
        filter(lambda test_case: not test_case.is_skipped, xml),
        key=lambda test_case: test_case.time,
        reverse=True,
    )

    pretty_print_tests(executed_test_cases[:5])

    if max_runtime is not None:
        if executed_test_cases[0].time > max_runtime:
            msg = textwrap.dedent(
                f"""
                ERROR: maximum allowed runtime exceeded
                allowed maximum runtime: {max_runtime}s
                maximum runtime: {executed_test_cases[0].time}s"""
            )
            print(msg)
            sys.exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("junit_xml")
    parser.add_argument("--max_runtime", required=False, default=None, type=float)
    args = parser.parse_args()

    main(args.junit_xml, args.max_runtime)
