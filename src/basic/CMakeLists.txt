target_sources(Octopus_lib PRIVATE
		accel.F90
		alloc_cache.F90
		alloc_cache_low.cc
		blacs.F90
		blacs_proc_grid.F90
		calc_mode_par.F90
		cgal_polyhedra.F90
		clblas.F90
		clblas_low.c
		clblast_low.c
		cublas.cc
		cuda.F90
		cuda_low.cc
		debug.F90
		debug_low.c
		gdlib.F90
		gdlib_f.c
		getopt_f.c
		global.F90
		hardware.F90
		heap.F90
		iihash.F90
		iihash_low.cc
		io.F90
		io_binary.c
		io_binary_f.F90
		io_csv.c
		io_csv_f.F90
		lattice_vectors.F90
		linked_list.F90
		list_node.F90
		loct.F90
		lookup.F90
		merge_sorted.F90
		messages.F90
		namespace.F90
		nvtx.F90
		nvtx_low.cc
		oct_f.c
		parser.F90
		profiling.F90
		recipes.c
		signals.c
		sihash.F90
		sihash_low.cc
		sort.F90
		sort_low.cc
		space.F90
		sphash.F90
		sphash_low.cc
		string.F90
		types.F90
		unit.F90
		unit_system.F90
		utils.F90
		varia.c
		varinfo.F90
		varinfo_low.c
		walltimer.F90
		write_iter.F90
		write_iter_low.cc
		)

target_sources(fortran_cli PRIVATE
		command_line.F90)

## Link targets
target_link_libraries(fortran_cli PRIVATE Octopus_base)

## External libraries
target_link_libraries(Octopus_lib PRIVATE GSL::gsl)
if (TARGET OpenMP::OpenMP_Fortran)
	target_link_libraries(Octopus_lib PRIVATE OpenMP::OpenMP_Fortran)
	target_compile_definitions(Octopus_lib PRIVATE HAVE_OPENMP)
endif ()

if (TARGET OpenCL::OpenCL)
	target_link_libraries(Octopus_lib PRIVATE
		OpenCL::OpenCL
		$<$<TARGET_EXISTS:clblast>:clblast>
		$<$<TARGET_EXISTS:clBLAS>:clBLAS>
		clFFT
		fortrancl)
endif ()

if (OCTOPUS_CUDA)
	target_link_libraries(Octopus_lib PRIVATE
		CUDA::cudart
		CUDA::cuda_driver
		CUDA::cublas
		CUDA::cufft
		CUDA::nvrtc
		CUDA::nvToolsExt)
endif ()

if (OCTOPUS_HIP)
	target_link_libraries(Octopus_lib PRIVATE
		hip::host
		roc::hipblas
		hip::hipfft
		hiprtc::hiprtc
		roc::roctx)
endif ()

if (TARGET netCDF::Fortran)
	target_link_libraries(Octopus_lib PRIVATE netCDF::Fortran)
endif ()

if (TARGET CGAL::CGAL)
	target_link_libraries(Octopus_lib PRIVATE CGAL::CGAL)
	target_sources(Octopus_lib PRIVATE
			cgal_polyhedra_low.cc
	)
endif ()

if (TARGET GD::GD)
	target_link_libraries(Octopus_lib PRIVATE GD::GD)
endif ()

if (TARGET verrou::verrou)
	if (OCTOPUS_NATIVE)
		message(WARNING "Valgrind does not support AVX512, but Octopus is being compiled with -march=native.")
	endif ()
	target_link_libraries(Octopus_lib PRIVATE verrou::verrou)
endif ()

if (TARGET LIKWID::LIKWID)
	target_link_libraries(Octopus_lib PRIVATE LIKWID::LIKWID)
endif ()

if (OCTOPUS_INSTALL)
	install(TARGETS Octopus_lib)
endif ()
