!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> this module contains the low-level part of the output system
module output_low_oct_m
  use debug_oct_m
  use global_oct_m
  use mesh_oct_m
  use messages_oct_m

  implicit none

  private
  public ::              &
    output_t,            &
    output_bgw_t,        &
    output_me_t,         &
    get_filename_with_spin

  !>@brief Output information for matrix elements
  type output_me_t
    logical, public :: what(MAX_OUTPUT_TYPES)               !< what to output
    !> If output_ksdipole, this number sets up which matrix elements will
    !! be printed: e.g. if ksmultipoles = 3, the dipole, quadrupole and
    !! octopole matrix elements (between Kohn-Sham or single-particle orbitals).
    !! In 2D, only the dipole moments are printed.
    integer :: ks_multipoles

    integer :: st_start                     !< Start index for the output
    integer :: st_end                       !< Stop index for the output
    integer :: nst                          !< Number of states computed
  end type output_me_t

  !>@brief Output information for BerkeleyGW
  type output_bgw_t
    !private
    integer           :: nbands
    integer           :: vxc_diag_nmin
    integer           :: vxc_diag_nmax
    integer           :: vxc_offdiag_nmin
    integer           :: vxc_offdiag_nmax
    logical           :: complex
    character(len=80) :: wfn_filename
    logical           :: calc_exchange
    logical           :: calc_vmtxel
    integer           :: vmtxel_ncband
    integer           :: vmtxel_nvband
    real(real64)      :: vmtxel_polarization(3)
  end type output_bgw_t


  !> @brief output handler class
  !!
  type output_t
    !private
    !> General output variables:
    logical, public    :: what(MAX_OUTPUT_TYPES)             !< what to output
    integer(int64), public :: how(0:MAX_OUTPUT_TYPES)           !< how to output

    type(output_me_t) :: me        !< this handles the output of matrix elements

    ! These variables fine-tune the output for some of the possible output options:
    integer, public :: output_interval(0:MAX_OUTPUT_TYPES)     !< output every iter
    integer, public :: restart_write_interval
    logical, public :: duringscf
    character(len=80), public :: wfs_list  !< If output_wfs, this list decides which wavefunctions to print.
    character(len=MAX_PATH_LEN), public :: iter_dir  !< The folder name, if information will be output while iterating.

    type(mesh_plane_t) :: plane    !< This is to calculate the current flow across a plane
    type(mesh_line_t)  :: line     !< or through a line (in 2D)

    type(output_bgw_t) :: bgw      !< parameters for BerkeleyGW output

  contains
    procedure :: what_now => output_what_now
    procedure :: anything_now => output_anything_now
  end type output_t

contains

  ! ---------------------------------------------------------

  function output_what_now(this, what_id, iter) result(write_now)
    class(output_t), intent(in) :: this
    integer(int64),     intent(in) :: what_id
    integer,         intent(in) :: iter

    logical :: write_now

    write_now = .false.
    if ((what_id > 0) .and. (this%output_interval(what_id) > 0)) then
      if (this%what(what_id) .and. (iter == -1 .or. mod(iter, this%output_interval(what_id)) == 0)) then
        write_now = .true.
      end if
    end if

  end function output_what_now

  ! ---------------------------------------------------------
  !> return true if any output is to be written now
  logical function output_anything_now(this, iter)
    class(output_t), intent(in) :: this
    integer,         intent(in) :: iter

    integer(int64) :: what_it

    PUSH_SUB(output_anything_now)

    output_anything_now = .false.
    do what_it = lbound(this%output_interval, 1), ubound(this%output_interval, 1)
      if (this%what_now(what_it, iter)) then
        output_anything_now = .true.
        exit
      end if
    end do

    POP_SUB(output_anything_now)
  end function output_anything_now

  !>@brief Returns the filame as output, or output-spX is spin polarized
  function get_filename_with_spin(output, nspin, spin_index) result(filename)
    character(len=*),   intent(in) :: output
    integer,            intent(in) :: nspin
    integer,            intent(in) :: spin_index
    character(len=MAX_PATH_LEN)    :: filename

    if (nspin == 1) then
      write(filename, fmt='(a)') trim(output)
    else
      write(filename, fmt='(a,a,i1)') trim(output), '-sp', spin_index
    end if
  end function get_filename_with_spin

end module output_low_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
