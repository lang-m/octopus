configure_file(config.h.in config.h)
file(READ ${CMAKE_CURRENT_BINARY_DIR}/config.h config_h_content)
# Remove all C style comments of \* *\
string(REGEX REPLACE "/\\\*[^\*]*\\\*/" "" config_f90_content ${config_h_content})
file(CONFIGURE OUTPUT config_F90.h CONTENT "@config_f90_content@")
target_include_directories(Octopus_base INTERFACE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)
