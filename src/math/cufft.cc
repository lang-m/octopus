/*
 Copyright (C) 2016 X. Andrade

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

*/

#include <config.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <fortran_types.h>

#ifdef HAVE_CUDA
#ifdef HAVE_HIP
#include <hip/hip_runtime.h>
#include <hipfft/hipfft.h>
#define cudaStream_t hipStream_t
#define CUdeviceptr hipDeviceptr_t
// https://rocm.docs.amd.com/projects/HIPIFY/en/latest/tables/CUFFT_API_supported_by_HIP.html
#define CUFFT_ALLOC_FAILED HIPFFT_ALLOC_FAILED
#define cufftDestroy hipfftDestroy
#define cufftDoubleComplex hipfftDoubleComplex
#define cufftDoubleReal hipfftDoubleReal
#define cufftExecD2Z hipfftExecD2Z
#define cufftExecZ2D hipfftExecZ2D
#define cufftExecZ2Z hipfftExecZ2Z
#define CUFFT_FORWARD HIPFFT_FORWARD
#define cufftHandle hipfftHandle
#define CUFFT_INVERSE HIPFFT_BACKWARD
#define cufftPlan3d hipfftPlan3d
#define cufftResult hipfftResult
#define cufftSetStream hipfftSetStream
#define CUFFT_SUCCESS HIPFFT_SUCCESS
#define CUFFT_INVALID_PLAN HIPFFT_INVALID_PLAN
#define CUFFT_ALLOC_FAILED HIPFFT_ALLOC_FAILED
#define CUFFT_INVALID_VALUE HIPFFT_INVALID_VALUE
#define CUFFT_INTERNAL_ERROR HIPFFT_INTERNAL_ERROR
#define CUFFT_EXEC_FAILED HIPFFT_EXEC_FAILED
#define CUFFT_SETUP_FAILED HIPFFT_SETUP_FAILED
#define CUFFT_INVALID_SIZE HIPFFT_INVALID_SIZE
#define CUFFT_INCOMPLETE_PARAMETER_LIST HIPFFT_INCOMPLETE_PARAMETER_LIST
#define CUFFT_INVALID_DEVICE HIPFFT_INVALID_DEVICE
#define CUFFT_PARSE_ERROR HIPFFT_PARSE_ERROR
#define CUFFT_NO_WORKSPACE HIPFFT_NO_WORKSPACE
#define CUFFT_NOT_IMPLEMENTED HIPFFT_NOT_IMPLEMENTED
#define CUFFT_NOT_SUPPORTED HIPFFT_NOT_SUPPORTED
#define cufftType hipfftType
#else
#include <cuda.h>
#include <cufft.h>
#endif
#else
typedef int cufftHandle;
typedef int CUdeviceptr;
typedef int cudaStream_t;
#endif

#define CUFFT_SAFE_CALL(x) cufft_safe_call((x), #x, __FILE__, __LINE__)

#ifdef HAVE_CUDA

static cufftResult cufft_safe_call(cufftResult safe_call_result,
                                   const char *call,
                                   const char *file,
                                   const int line) {
  if (safe_call_result != CUFFT_SUCCESS) {

    std::string error_code;
    std::string error_desc;

    switch (safe_call_result) {
    case CUFFT_SUCCESS:
      error_code = "CUFFT_SUCCESS";
      error_desc = "The cuFFT operation was successful";
      break;
    case CUFFT_INVALID_PLAN:
      error_code = "CUFFT_INVALID_PLAN";
      error_desc = "cuFFT was passed an invalid plan handle";
      break;
    case CUFFT_ALLOC_FAILED:
      error_code = "CUFFT_ALLOC_FAILED";
      error_desc = "cuFFT failed to allocate GPU or CPU memory";
      break;
    case CUFFT_INVALID_VALUE:
      error_code = "CUFFT_INVALID_VALUE";
      error_desc = "User specified an invalid pointer or parameter";
      break;
    case CUFFT_INTERNAL_ERROR:
      error_code = "CUFFT_INTERNAL_ERROR";
      error_desc = "Driver or internal cuFFT library error";
      break;
    case CUFFT_EXEC_FAILED:
      error_code = "CUFFT_EXEC_FAILED";
      error_desc = "Failed to execute an FFT on the GPU";
      break;
    case CUFFT_SETUP_FAILED:
      error_code = "CUFFT_SETUP_FAILED";
      error_desc = "The cuFFT library failed to initialize";
      break;
    case CUFFT_INVALID_SIZE:
      error_code = "CUFFT_INVALID_SIZE";
      error_desc = "User specified an invalid transform size";
      break;
    case CUFFT_INCOMPLETE_PARAMETER_LIST:
      error_code = "CUFFT_INCOMPLETE_PARAMETER_LIST";
      error_desc = "Missing parameters in call";
      break;
    case CUFFT_INVALID_DEVICE:
      error_code = "CUFFT_INVALID_DEVICE";
      error_desc = "Execution of a plan was on different GPU than plan creation";
      break;
    case CUFFT_PARSE_ERROR:
      error_code = "CUFFT_PARSE_ERROR";
      error_desc = "Internal plan database error";
      break;
    case CUFFT_NO_WORKSPACE:
      error_code = "CUFFT_NO_WORKSPACE";
      error_desc = "No workspace has been provided prior to plan execution";
      break;
    case CUFFT_NOT_IMPLEMENTED:
      error_code = "CUFFT_NOT_IMPLEMENTED";
      error_desc = "Function does not implement functionality for parameters given.";
      break;
    case CUFFT_NOT_SUPPORTED:
      error_code = "CUFFT_NOT_SUPPORTED";
      error_desc = "Operation is not supported for parameters given.";
      break;
    default:
      error_code = "UNKOWN";
      error_desc = "Unknown error code. Potentially deprecated value for cufftResult.";
      break;
    }

    std::cerr << file << ":" << line << ": error: " << error_code << " (" << safe_call_result << ") while calling\n"
              << "    " << call << "\n"
              << error_desc << std::endl;

    exit(1);
  }
  return safe_call_result;
}

#endif

extern "C" void FC_FUNC_(cuda_fft_plan3d,
                         CUDA_FFT_PLAN3D)(cufftHandle **plan, fint *nx,
                                          fint *ny, fint *nz, fint *type,
                                          cudaStream_t **stream) {
  *plan = new cufftHandle;
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftPlan3d(*plan, *nx, *ny, *nz, (cufftType)*type));
  CUFFT_SAFE_CALL(cufftSetStream(**plan, **stream));
#endif
}

extern "C" void FC_FUNC_(cuda_fft_destroy,
                         CUDA_FFT_DESTROY)(cufftHandle **plan) {
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftDestroy(**plan));
#endif
  delete *plan;
}

extern "C" void FC_FUNC_(cuda_fft_execute_d2z,
                         CUDA_FFT_EXECUTE_D2Z)(cufftHandle **plan,
                                               CUdeviceptr **idata,
                                               CUdeviceptr **odata,
                                               cudaStream_t **stream) {
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftSetStream(**plan, **stream));
  CUFFT_SAFE_CALL(cufftExecD2Z(**plan, (cufftDoubleReal *)**idata,
                               (cufftDoubleComplex *)**odata));
#endif
}

extern "C" void FC_FUNC_(cuda_fft_execute_z2d,
                         CUDA_FFT_EXECUTE_Z2D)(cufftHandle **plan,
                                               CUdeviceptr **idata,
                                               CUdeviceptr **odata,
                                               cudaStream_t **stream) {
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftSetStream(**plan, **stream));
  CUFFT_SAFE_CALL(cufftExecZ2D(**plan, (cufftDoubleComplex *)**idata,
                               (cufftDoubleReal *)**odata));
#endif
}

extern "C" void FC_FUNC_(cuda_fft_execute_z2z_forward,
                         CUDA_FFT_EXECUTE_Z2Z_FORWARD)(cufftHandle **plan,
                                                       CUdeviceptr **idata,
                                                       CUdeviceptr **odata,
                                                       cudaStream_t **stream) {
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftSetStream(**plan, **stream));
  CUFFT_SAFE_CALL(cufftExecZ2Z(**plan, (cufftDoubleComplex *)**idata,
                               (cufftDoubleComplex *)**odata, CUFFT_FORWARD));
#endif
}

extern "C" void FC_FUNC_(cuda_fft_execute_z2z_backward,
                         CUDA_FFT_EXECUTE_Z2Z_BACKWARD)(cufftHandle **plan,
                                                        CUdeviceptr **idata,
                                                        CUdeviceptr **odata,
                                                        cudaStream_t **stream) {
#ifdef HAVE_CUDA
  CUFFT_SAFE_CALL(cufftSetStream(**plan, **stream));
  CUFFT_SAFE_CALL(cufftExecZ2Z(**plan, (cufftDoubleComplex *)**idata,
                               (cufftDoubleComplex *)**odata, CUFFT_INVERSE));
#endif
}
