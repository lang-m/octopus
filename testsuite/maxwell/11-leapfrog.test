# -*- coding: utf-8 mode: shell-script -*-

Test       : Free Maxwell propagation through a linear medium with PML boundaries, using the Leapfrog propagator
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : Yes

Input      : 11-leapfrog.01-fullrun.inp
Precision: 1.01e-04
match ;     Tot. Maxwell energy [step 0]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.20199
Precision: 2.5e-15
match ;     Tot. Maxwell energy [step 50]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.2059034805544106
Precision: 2.8e-15
match ;     Tot. Maxwell energy [step 100]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.2058355618620914
Precision: 3.51e-15
match ;     Tot. Maxwell energy [step 200]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 206, 3) ; 0.2058173908402738

Precision: 9.20e-18
match ;     Ez (x=6,y=  0,z=  0) [step 100]     ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2355, 3) ; 8.403392460868821e-05
Precision: 1.28e-17
match ;     Ez (x=14,y=8,z=  0) [step 100]     ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3299, 3) ; 0.000256253236203351
Precision: 1.45e-15
match ;     Ez (x=14,y=8,z=  0) [step 200]     ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 3299, 3) ; 0.029055409380688305

Input      : 11-leapfrog.02-pml_fullrun.inp
Precision: 5.73e-15
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.104507601715533
Precision: 1.06e-15
match ;    Tot. Maxwell energy [step 50]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.1062409057823512
Precision: 1.39e-15
match ;    Tot. Maxwell energy [step 100]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.1062212285943948
Precision: 5.5e-17
match ;    Tot. Maxwell energy [step 200]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 206, 3) ; 0.001247461623189457

Precision: 2.61e-15
match ;    Ez (x=6,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2355, 3) ; 0.052189523413758904
Precision: 1.08e-18
match ;    Ez (x=14,y=8,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3299, 3) ; 1.42987341215334e-07
Precision: 6.2e-18
match ;    Ez (x=14,y=8,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 3299, 3) ; 1.8600808367821203e-05

Input      : 11-leapfrog.03-pml_medium_fullrun.inp
Precision: 5.73e-15
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.104507601715533
Precision: 1.28e-15
match ;    Tot. Maxwell energy [step 50]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.09303517141611349
Precision: 2.02e-15
match ;    Tot. Maxwell energy [step 100]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.08103760890142292
Precision: 3.01e-15
match ;    Tot. Maxwell energy [step 200]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 206, 3) ; 0.03847892398430394

Precision: 6.78e-18
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 35, 3) ; 5.404632545709645e-06
Precision: 1.40e-18
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3283, 3) ; -3.4395363831810904e-06
Precision: 1.06e-14
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1659, 3) ; 0.023793474895441
Precision: 8.63e-15
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1712, 3) ; 0.04536332415610895
Precision: 2.63e-15
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2374, 3) ; 0.052630453047115104
Precision: 2.65e-16
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2297, 3) ; -0.000393159162299625
Precision: 4.2e-17
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 35, 3) ; 1.2190615896989001e-05
Precision: 1.5e-17
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 3283, 3) ; 3.55698896049727e-05
Precision: 1.19e-15
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 1659, 3) ; -0.00171027426133015
Precision: 9.68e-16
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 1712, 3) ; 0.00128492019822226
Precision: 1.42e-15
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 2374, 3) ; -0.0022146353331934
Precision: 5.01e-14
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 2297, 3) ; -0.1002093547533

Input      : 11-leapfrog.04-pml_medium_restart_part1.inp
Input      : 11-leapfrog.05-pml_medium_restart_part2.inp

Precision: 5.73e-15
match ;    Tot. Maxwell energy [step 0]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 6, 3) ; 0.104507601715533
Precision: 1.28e-15
match ;    Tot. Maxwell energy [step 50]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 56, 3) ; 0.09303517141611349
Precision: 2.00e-15
match ;    Tot. Maxwell energy [step 100]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 106, 3) ; 0.0810376089014229
Precision: 3.01e-15
match ;    Tot. Maxwell energy [step 200]    ; LINEFIELD(Maxwell/td.general/maxwell_energy, 206, 3) ; 0.03847892398430394

Precision: 6.78e-18
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 35, 3) ; 5.404632545709645e-06
Precision: 1.40e-18
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 3283, 3) ; -3.4395363831810904e-06
Precision: 1.06e-14
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 100]    ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1659, 3) ; 0.023793474895441
Precision: 8.63e-15
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 1712, 3) ; 0.04536332415610895
Precision: 2.63e-15
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2374, 3) ; 0.052630453047115104
Precision: 2.65e-16
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/e_field-z\.z=0, 2297, 3) ; -0.000393159162299625
Precision: 4.2e-17
match ;    Ez at PML border (x=-14,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 35, 3) ; 1.2190615896989001e-05
Precision: 1.50e-17
match ;    Ez at PML border (x= 14,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 3283, 3) ; 3.55698896049727e-05
Precision: 1.19e-15
match ;    Ez in medium, center (x=  0,y=  0,z=  0) [step 200]    ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 1659, 3) ; -0.00171027426133015
Precision: 9.68e-16
match ;    Ez in medium, focus (x=0.5,y=-2.5,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 1712, 3) ; 0.00128492019822226
Precision: 1.42e-15
match ;    Ez in vacuum, pw max (x=6,y=9.5,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 2374, 3) ; -0.0022146353331934
Precision: 5.01e-14
match ;    Ez in vacuum, focus (x=5.5,y=0,z=  0) [step 200]   ; LINEFIELD(Maxwell/output_iter/td.0000200/e_field-z\.z=0, 2297, 3) ; -0.1002093547533
