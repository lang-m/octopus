 !> @brief Exposes all unit tests associated with the opt_control folder to fortuno cmd line app
module driver_opt_control_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_opt_control

contains

    function testsuite_opt_control() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_opt_control

end module driver_opt_control_oct_m