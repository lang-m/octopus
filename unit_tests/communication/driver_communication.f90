 !> @brief Exposes all unit tests associated with the communication folder to fortuno cmd line app
module driver_communication_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_communication

contains

    function testsuite_communication() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_communication

end module driver_communication_oct_m
