 !> @brief Exposes all unit tests associated with the photons folder to fortuno cmd line app
module driver_photons_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_photons

contains

    function testsuite_photons() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_photons

end module driver_photons_oct_m