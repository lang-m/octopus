 !> @brief Exposes all unit tests associated with the td folder to fortuno cmd line app
module driver_td_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_td

contains

    function testsuite_td() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_td

end module driver_td_oct_m