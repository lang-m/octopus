!> @brief Exposes all unit tests associated with the basic folder to fortuno cmd line app
module driver_basic_oct_m
    use fortuno_interface_m, only: test_list

    ! Test modules for modules defined in the basic folder
    use testsuite_parser_oct_m
    use testsuite_sort_oct_m

    implicit none
    private
    public :: testsuite_basic

contains

    function testsuite_basic() result(tests)
        type(test_list) :: tests

        tests = test_list([&
            testsuite_parser(),&
            testsuite_sort()&
        ])

    end function testsuite_basic

end module driver_basic_oct_m
