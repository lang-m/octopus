!! Copyright (C) 2024 Alex Buccheri

!> Unit test routines from the `parser_oct_m` module
module testsuite_parser_oct_m
  use, intrinsic :: iso_fortran_env, only: real64
  use fortuno_interface_m
  use global_oct_m,        only: global_init, conf
  use namespace_oct_m,     only: namespace_t

  use parser_oct_m

  implicit none
  private
  public :: testsuite_parser

  type(namespace_t)              :: namespace
  character(len=:), allocatable  :: parser_log

contains

  !> @brief Returns a suite instance, wrapped as test_item
  function testsuite_parser() result(res)
    type(test_item), allocatable :: res

    res = suite("basic/parser", test_list([&
        test_case("test_parse_input_string", test_parse_input_string)&
      ]))

  end function testsuite_parser

  !> @brief Set up module-scoped variables, required by the parser
  subroutine setup()
    integer                       :: i_parent_end
    character(len=:), allocatable :: cmake_build_dir

    namespace = namespace_t("test/basic/test_parser")

    ! Initialise global conf and mpi_world objects
    call global_init(communicator=global_comm())

    ! Convention that share/ lives in the top level of the build directory
    i_parent_end = index(conf%share, '/', .true.)
    cmake_build_dir = conf%share(1:i_parent_end-1)
    parser_log = cmake_build_dir // '/test_parser.log'

  end subroutine setup

  subroutine test_parse_input_string()
    character(len=:), allocatable :: inp_str
    integer                       :: dim, periodic_dim, ierr
    integer, parameter            :: default_int = 0

    ! This is fine for a single test, but ideally needs moving to a fixture
    if (.not. allocated(parser_log)) call setup()

    ! Initialise the parser with keys
    call parser_initialize_symbol_table(parser_log)

    ! Define some input options as a string
    inp_str = 'PeriodicDimensions = 3' // new_line('a') &
      // 'Dimensions = 1'

    ierr = parse_input_string(inp_str, set_used = 0)

    call parse_variable(namespace, 'Dimensions', default_int, dim)
    call parse_variable(namespace, 'PeriodicDimensions', default_int, periodic_dim)

    call check(periodic_dim == 3)
    call check(dim == 1)

    ! Reset PeriodicDimensions
    inp_str = 'PeriodicDimensions = 1'

    ierr = parse_input_string(inp_str, set_used = 0)
    call parse_variable(namespace, 'Dimensions', default_int, dim)
    call parse_variable(namespace, 'PeriodicDimensions', default_int, periodic_dim)

    call check(periodic_dim == 1)
    call check(dim == 1)

    ! Free the symbol table
    call parse_end()

    ! Reinitialise the parser
    call parser_initialize_symbol_table(parser_log)

    call parse_variable(namespace, 'Dimensions', default_int, dim)
    call parse_variable(namespace, 'PeriodicDimensions', default_int, periodic_dim)

    call check(periodic_dim == default_int, msg='PeriodicDimensions should fall back to the default')
    call check(dim == default_int, msg='Dimensions should fall back to the default')

    ! Free the symbol table
    call parse_end()

  end subroutine test_parse_input_string

end module testsuite_parser_oct_m
