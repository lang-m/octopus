 !> @brief Exposes all unit tests associated with the classical folder to fortuno cmd line app
module driver_classical_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_classical

contains

    function testsuite_classical() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_classical

end module driver_classical_oct_m
