 !> @brief Exposes all unit tests associated with the maxwell folder to fortuno cmd line app
module driver_maxwell_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_maxwell

contains

    function testsuite_maxwell() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_maxwell

end module driver_maxwell_oct_m