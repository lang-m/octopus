!> @brief The mocking module provides convenience routines that wrap the  
!! subroutines required to generate fundamental quantitities and data structures in
!! Octopus, for which the majority of the routines in the code depend upon.
!! 
!! It does so by hiding the population of the parser symbol table, on which many
!! class initialisers depend upon, removing the need for the definition of a `inp`ut
!! file defined on disk.
!!
!! The routines here are meant to be limited in scope, such that one can mock the most
!! of the objects required to perform unit testing in Octopus. These should cover:

!!   Name         Description                Class
!!   -----------------------------------------------------
!!    mc       Multi-comms                 multicomm_t
!!    ions     Atoms and species           ions_t
!!    gr       Grid                        mesh_t/grid_t
!!    space    Dimensionality              electron_space_t
!!    eigens   Eigensolver settings        eigensolver_t    
!!    st       States                      states_elec_t
!!    hm       Hamiltonian                 hamiltonian_elec_t
!!    wfs      Batched wavefunctions       batch_t
!!    kpoints  k-points                    kpoints_t
!!
!! As the number of mocked routines grows, one may consider splitting into several modules
!! and retain this as an entry point.
module mock_oct_m
    use, intrinsic :: iso_fortran_env, only: real64, int64
    use calc_mode_par_oct_m,   only: calc_mode_par, P_STRATEGY_STATES, P_STRATEGY_KPOINTS, &
                                     P_STRATEGY_DOMAINS, P_STRATEGY_MAX
    use electron_space_oct_m,  only: electron_space_t
    use grid_oct_m,            only: grid_t, grid_end, grid_init_stage_1, grid_init_stage_2
    use ions_oct_m,            only: ions_t
    use loct_oct_m,            only: loct_mkdir
    use mpi_oct_m,             only: mpi_grp_t, mpi_world
    use multicomm_oct_m,       only: multicomm_t, multicomm_init
    use namespace_oct_m,       only: namespace_t
    use parser_oct_m,          only: parse_input_string

    implicit none
    private
    public :: block_line_str, &
              initialise_multicomm, &
              mock_dummy_ion, &
              mock_grid_given_input, &
              mock_rectangular_grid, &
              mock_space

    !> Compact way of defining new_line('a')
    character(len=1), public, parameter :: nl = achar(10) 

    interface block_line_str
        module procedure :: block_line_str_real64
    end interface 

contains

    !> @brief Construct a block line string for an inp file.
    !! For example: "0.05 | 0.05 | 0.01"
    function block_line_str_real64(x) result(str)

        real(real64), intent(in) :: x(:)
        character(len=32) :: temp_str
        character(len=:), allocatable :: str

        integer :: i

        write(temp_str, '(F6.2)') x(1)
        str = trim(temp_str)

        do i = 2, size(x)
            write(temp_str, '(F6.2)') x(i)
            str = trim(str) // ' | ' // trim(temp_str)
        end do
        
    end function block_line_str_real64


    !> @brief Wrapper to initialise multicomm
    subroutine initialise_multicomm(namespace, mpi_world, np_global, nst, nik, mc)
        type(namespace_t), intent(in   ) :: namespace  !< Namespace
        type(mpi_grp_t),   intent(in   ) :: mpi_world  !< MPI world container
        integer(int64),    intent(in   ) :: np_global  !< Number of grid points + boundary (and ghost?)
        integer(int64),    intent(in   ) :: nst        !< Number of states
        integer(int64),    intent(in   ) :: nik        !< Number of k-points
        type(multicomm_t), intent(inout) :: mc         !< multicomm container

        integer(int64), parameter  :: large_number = 100000_int64
        integer(int64)             :: index_range(P_STRATEGY_MAX)

        index_range = [np_global, nst, nik, large_number]

        ! Set "valid" parallelisation strategies
        call calc_mode_par%set_parallelization(P_STRATEGY_DOMAINS, .false.)
        call calc_mode_par%set_parallelization(P_STRATEGY_KPOINTS, .false.)
        ! This is essentially the only call in ground_state_run_init() of run
        call calc_mode_par%set_parallelization(P_STRATEGY_STATES, .false.)

        ! Initialise communicators in `mc`
        call multicomm_init(mc, &
            namespace, &
            mpi_world, &
            calc_mode_par, &
            mpi_world%size, & 
            index_range, &
            [5000, 1, 1, 1])

    end subroutine initialise_multicomm


    !> @brief Generate an ions instance with a single dummy atom.
    !! At least one ion is required for the generation of a mesh/grid.
    function mock_dummy_ion(namespace) result(ion)
        type(namespace_t), intent(in) :: namespace  !< Namespace
        type(ions_t), pointer     :: ion

        character(len=:), allocatable :: inp_str
        integer                       :: ierr

        ! Ions requires species and coordinates as in input
        inp_str = '%Coordinates'                                                  // nl // &
        ' "dummy" | 0.0 | 0.0 | 0.0'                                              // nl // &
        '%'                                                                       // nl // &
        '%Species'                                                                // nl // &
        ' "dummy" | species_user_defined | potential_formula | "0" | valence | 1' // nl // &
        '%'
        ierr = parse_input_string(inp_str, set_used=0)

        ! The constructor populates lattice, symmetries, positions and all other attributes
        ion => ions_t(namespace)

    end function mock_dummy_ion


    !> @brief Mock an N-dimensional grid, with correct MPI distribution.
    subroutine mock_grid_given_input(namespace, space, ions, mc, inp_str, gr)
        type(namespace_t),       intent(in )  :: namespace  !< Namespace
        type(electron_space_t),  intent(in )  :: space      !< electron space
        type(ions_t),            intent(in )  :: ions       !< ions
        type(multicomm_t),       intent(out)  :: mc         !< multi-comms
        character(len=*),        intent(in )  :: inp_str    !< Input string for grid
        type(grid_t),            intent(out)  :: gr         !< grid

        integer :: ierr

        ierr = parse_input_string(trim(inp_str), set_used=0)

        ! Initialise 'gr'id
        call grid_init_stage_1(gr, namespace, space, ions%symm, ions%latt, ions%natoms, ions%pos)

        ! Domain decomposition
        call initialise_multicomm(namespace, mpi_world, gr%np_global, 1_int64, 1_int64, mc)
        call grid_init_stage_2(gr, namespace, space, mc)

    end subroutine mock_grid_given_input


    !> @brief Mock a rectangular (parallelepiped) grid.
    subroutine mock_rectangular_grid(namespace, space, ions, spacing, lsize, mc, gr)
        type(namespace_t),       intent(in )  :: namespace   !< Namespace
        type(electron_space_t),  intent(in )  :: space       !< electron space
        type(ions_t),            intent(in )  :: ions        !< ions
        real(real64),            intent(in )  :: spacing(:)  !< grid spacing
        real(real64),            intent(in )  :: lsize(:)    !< Half-lengths of box
        type(multicomm_t),       intent(out)  :: mc          !< multi-comms
        type(grid_t),            intent(out)  :: gr          !< grid

        character(len=:), allocatable :: inp_str

        inp_str = &
        '%Spacing'                   // nl // &
        block_line_str(spacing)      // nl // &
        '%'                          // nl // &
        'BoxShape = parallelepiped'  // nl // &
        '%Lsize'                     // nl // &
        block_line_str(lsize)        // nl // &
        '%'                          // nl // &
        'RestartWrite = false'

        call mock_grid_given_input(namespace, space, ions, mc, inp_str, gr)

    end subroutine mock_rectangular_grid


    !> @brief Mock an electron space instance
    subroutine mock_space(namespace, ndim, nperiodic, nspin, space)
        type(namespace_t),      intent(in )  :: namespace  !< Namespace
        integer,                intent(in )  :: ndim      !< System dimensions
        integer,                intent(in )  :: nperiodic !< Number of periodic dimensions
        integer,                intent(in )  :: nspin     !< Spin
        type(electron_space_t), intent(out)  :: space     !< electron space

        character(len=:), allocatable :: inp_str
        integer                       :: ierr
        character(len=1)              :: ndim_char, nperiodic_char, nspin_char

        write(ndim_char, '(I1)') ndim
        write(nperiodic_char, '(I1)') nperiodic
        write(nspin_char, '(I1)') nspin

        inp_str = &
        'Dimensions = '         // trim(ndim_char)      // nl // &
        'PeriodicDimensions = ' // trim(nperiodic_char) // nl // &
        'SpinComponents = '     // trim(nspin_char)  

        ierr = parse_input_string(inp_str, set_used=0)

        space = electron_space_t(namespace)

    end subroutine mock_space


end module mock_oct_m
