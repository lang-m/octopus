 !> @brief Exposes all unit tests associated with the basis_set folder to fortuno cmd line app
module driver_basis_set_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_basis_set

contains

    function testsuite_basis_set() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_basis_set

end module driver_basis_set_oct_m
