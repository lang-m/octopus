target_sources(Octopus_test_suite PRIVATE
		driver_boxes.f90   # Drivers are not added to test ITEMS below
)
foreach (test IN ITEMS
)
	Octopus_add_fortuno_test(TARGET Octopus_test_suite
			TEST_NAME ${test}
	)
endforeach ()
