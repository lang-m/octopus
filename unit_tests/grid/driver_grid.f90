!> @brief Exposes all unit tests associated with the grid folder to fortuno cmd line app
module driver_grid_oct_m
    use fortuno_interface_m, only: test_list

    use testsuite_centroids_oct_m
    use testsuite_kmeans_clustering_oct_m

    implicit none
    private
    public :: testsuite_grid

contains

    function testsuite_grid() result(tests)
        type(test_list) :: tests

        tests = test_list([&
            testsuite_centroids(), &
            testsuite_kmeans_clustering() &
                ])

    end function testsuite_grid

end module driver_grid_oct_m
