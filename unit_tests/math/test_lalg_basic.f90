!> Unit test routines from the `lalg_basic_oct_m` module
module testsuite_lalg_basic_oct_m
    use, intrinsic :: iso_fortran_env
    use fortuno_interface_m
    use global_oct_m,        only: global_init, conf

    use lalg_basic_oct_m

    implicit none
    private
    public :: testsuite_lalg_basic

  contains

     !> @brief Returns a suite instance, wrapped as test_item
    function testsuite_lalg_basic() result(res)
      type(test_item), allocatable :: res

      res = suite("math/lalg_basic", test_list([&
          test_case("test_gemm_cc_1_2", test_gemm_cc_1_2),  &
          test_case("test_lalg_gemm_simple", test_lalg_gemm_simple) &
        ]))

    end function testsuite_lalg_basic


    subroutine test_gemm_cc_1_2()
        real(real64)    :: A(2, 3), B(3, 2), result(3, 3)
        complex(real64) :: C(2, 3), D(3, 2), result2(3, 3)
        complex(real64), parameter :: alpha_cmplx = cmplx(1.0_real64, .0_real64)
        complex(real64), parameter :: beta_cmplx =  cmplx(0.0_real64, 0.0_real64)
        integer :: i

        ! Test with real arrays
        A = transpose(reshape(&
            [1, 2, 3, &
            4, 5, 6 ], [3, 2]))

        B = transpose(reshape(&
            [7, 8,  &
             9, 10, &
             11, 12], [2, 3]))

        call lalg_gemm_cc(size(A, 2), size(B, 1), size(A, 1), 1.00_real64, &
          A, B, 0.0_real64, result)
        call check(all_close(result, matmul(transpose(A), transpose(B))), msg='A^T B^T')

        ! Test with complex arrays
        C = transpose(reshape(&
            [cmplx(1,  2), cmplx(2, -1), cmplx(3,  4), &
             cmplx(4, -2), cmplx(5,  3), cmplx(6, -1)], [3, 2]))

        D = transpose(reshape(&
            [cmplx(7,  1), cmplx(8, -2), &
             cmplx(9, -3), cmplx(10, 4), &
             cmplx(11, 2), cmplx(12, -1)], [2, 3]))

        call lalg_gemm_cc(size(C, 2), size(D, 1), size(C, 1), alpha_cmplx, C, D, beta_cmplx, result2)
        call check(all_close(result2, matmul(transpose(conjg(C)), transpose(conjg(D)))), msg='C^dagger D^dagger')

    end subroutine test_gemm_cc_1_2


    subroutine test_lalg_gemm_simple()
        real(real64) :: a(4, 4), b(4, 3), c(3, 4)  !< Arbitrarily-chosen matrices
        real(real64) :: result(4, 3)

        a = transpose(reshape(&
            [9.54_real64, 2.61_real64, 1.83_real64, 1.22_real64, &
             6.70_real64, 1.42_real64, 0.92_real64, 8.68_real64, &
             7.38_real64, 5.88_real64, 0.84_real64, 5.74_real64, &
             8.58_real64, 9.90_real64, 6.74_real64, 4.43_real64], [4, 4]))

        b = transpose(reshape(&
             [7.60_real64, 9.92_real64, 0.77_real64, &
              7.87_real64, 5.15_real64, 1.69_real64, &
              2.58_real64, 0.08_real64, 7.22_real64, &
              0.80_real64, 4.80_real64, 1.60_real64], [3, 4]))

        c = transpose(reshape(&
              [0.96_real64, 9.49_real64, 0.65_real64, 9.02_real64, &
               9.13_real64, 9.91_real64, 4.45_real64, 7.19_real64, &
               0.94_real64, 7.31_real64, 0.90_real64, 2.00_real64], [4, 3]))

        call lalg_gemm(a, b, result)
        call check(all_close(result, matmul(a, b)), msg='A B')

        call lalg_gemm(a, b, result, transa='T')
        call check(all_close(result, matmul(transpose(a), b)), msg='A^T B')

        call lalg_gemm(a, c, result, transb='T')
        call check(all_close(result, matmul(a, transpose(c))), msg='A C^T')

        call lalg_gemm(a, c, result, transa='T', transb='T')
        call check(all_close(result, matmul(transpose(a), transpose(c))), msg='A^T C^T')

        call lalg_gemm(a, b, result, alpha=2.00_real64)
        call check(all_close(result, 2.00_real64 * matmul(a, b)), msg='alpha*A B')

        ! Add the product to the existing data in result
        result = 0.25_real64
        call lalg_gemm(a, b, result, beta=1.00_real64)
        call check(all_close(result, matmul(a, b) + 0.25_real64), msg='C += A B')

    end subroutine test_lalg_gemm_simple


end module testsuite_lalg_basic_oct_m
