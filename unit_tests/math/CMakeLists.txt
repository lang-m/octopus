target_sources(Octopus_test_suite PRIVATE
		driver_math.f90 # Drivers are not registered as test ITEMS   
		test_lalg_basic.f90
		test_lalg_adv.f90
		test_quickrnd.f90
)
foreach (test IN ITEMS
		math/lalg_basic
		math/lalg_adv
		math/quickrnd
)
	Octopus_add_fortuno_test(TARGET Octopus_test_suite
			TEST_NAME ${test}
	)
endforeach ()
