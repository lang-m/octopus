!> Unit test routines from the `quickrnd_oct_m` module
module testsuite_quickrnd_oct_m
  use, intrinsic :: iso_fortran_env
  use fortuno_interface_m
  use global_oct_m,        only: global_init, conf
  use sort_oct_m,          only: sort
  use quickrnd_oct_m

  implicit none
  private
  public :: testsuite_quickrnd

contains

   !> @brief Returns a suite instance, wrapped as test_item
  function testsuite_quickrnd() result(res)
    type(test_item), allocatable :: res

    res = suite("math/quickrnd", test_list([&
        test_case("test_reservoir_sampling_aexpj", test_reservoir_sampling_aexpj)&
      ]))

  end function testsuite_quickrnd


  subroutine test_reservoir_sampling_aexpj
    real(real64),   allocatable   :: weight(:)
    integer(int32), allocatable   :: reservoir(:), expected_reservoir(:)

    integer, parameter :: n = 100 !< population size
    integer, parameter :: m = 20  !< reservoir size
    integer(int32), parameter :: seed = 541428131 !< Hard-coded seed

    integer :: i
    logical :: has_duplicates

    allocate(weight(n), source=1.0_real64)
    ! Make indices 40:59 extremely probable to be selected
    weight(40:40+m-1) = 1.e7_real64
    allocate(reservoir(m))
    call reservoir_sampling_aexpj(weight, reservoir, seed_value=seed)

    call sort(reservoir)
    call check(reservoir(1) == 40, msg='Expect lowest value to be 40')
    call check(reservoir(m) == 59, msg='Expect largest value to be 59')

    ! Confirm no duplicates are present
    has_duplicates = .false.
    do i = 1, m - 1
      if (reservoir(i) == reservoir(i+1)) has_duplicates = .true.
    enddo
    call check(.not. has_duplicates, msg='Weighted sampling should return without replacement')

    ! Second choice of weights
    ! Make indices 1:10 and 91:100 extremely probable to be selected
    weight = 1.0_real64
    weight(1:10) = 1.e7_real64
    weight(91:100) = 1.e7_real64
    reservoir = 0
    call reservoir_sampling_aexpj(weight, reservoir, seed_value=seed)
    call sort(reservoir)
    allocate(expected_reservoir(m))
    expected_reservoir = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, &
                          91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
    call check(all(reservoir == expected_reservoir))

  end subroutine test_reservoir_sampling_aexpj

end module testsuite_quickrnd_oct_m
