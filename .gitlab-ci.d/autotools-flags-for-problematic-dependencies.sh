if [[ $CONFIGURE_FLAGS =~ "--with-elpa-prefix" ]] ; then
    echo "setting elpa flags manually"
    try_mpsd_elpa_version=`expr match "$MPSD_ELPA_ROOT" '.*/elpa-\([0-9.]\+\)'`
    if [ -n "$try_mpsd_elpa_version" ] ; then
        if [ -r $MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules/elpa.mod ]; then
            echo elpa with openmp
            export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa_openmp-$try_mpsd_elpa_version/modules"
            export LIBS_ELPA="-lelpa_openmp"
        elif [ -r $MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules/elpa.mod ] ; then
            echo elpa without openmp
            export FCFLAGS_ELPA="-I$MPSD_ELPA_ROOT/include/elpa-$try_mpsd_elpa_version/modules"
        fi
    fi
    unset try_mpsd_elpa_version
fi

# currently problematic with autotools. Investigate further.
# if [[ $CONFIGURE_FLAGS =~ "--with-pfft-prefix" ]] ; then
#     # set --with-pfft-prefix to yes to avoid that the m4 overwrites the manually set LIBS_PFFT
#     CONFIGURE_FLAGS=`echo $CONFIGURE_FLAGS | sed -Ee 's/--with-pfft-prefix[^ ]+ /--with-pfft-prefix=yes /'`
#     echo "setting pfft flags manually"
#     try_mpsd_pfft_version=`expr match "$MPSD_PFFT_ROOT" '.*/pfft-\([0-9.]\+\)'`
#     if [ -n "$try_mpsd_pfft_version" ] ; then
#         export FCFLAGS_PFFT="-I$MPSD_PFFT_ROOT/include -I$MPSD_FFTW_ROOT/include"
#         export LIBS_PFFT="-L$MPSD_FFTW_ROOT/lib -lfftw3_mpi -L$MPSD_PFFT_ROOT/lib -lpfft"
#     fi
#     unset try_mpsd_pfft_version
# fi
# 
# if [[ $CONFIGURE_FLAGS =~ "--with-berkeleygw-prefix" ]] ; then
#     echo "setting berkeleygw flags manually"
#     try_mpsd_berkeleygw_version=`expr match "$MPSD_BERKELEYGW_ROOT" '.*/berkeleygw-\([0-9.]\+\)'`
#     if [ -n "$try_mpsd_berkeleygw_version" ] ; then
#         # we provide berkeleygw compiled with openmp support as static library;
#         # we have to additionally link against gomp
#         # (the berkeleygw documentation does not mention any option to compile
#         # as a shared library)
#         export LIBS_BERKELEYGW="-L$with_berkeleygw_prefix/lib -lBGW_wfn -lgomp"
#         # do not set any FCFLAGS_BERKELEYGW because the m4 would anyway overwrite those
#     fi
#     unset try_mpsd_berkeleygw_version
# fi
# 
# if [[ $CONFIGURE_FLAGS =~ "--enable-mpi" ]] ; then
#     # enforce no dftbplus:
#     CONFIGURE_FLAGS=`echo $CONFIGURE_FLAGS | sed -Ee 's/--with-dftbplus-prefix[^ ]+ /--without-dftbplus /'`
#     # Reason: in the m4 Octopus with(out) MPI and DFTB+ with(out) MPI are mixed up
#     # and it seems too much work for me (Martin Lang) to fix this now; we were told
#     # that we can provide DFTB+ without MPI support even for the MPI toolchain
#     # because the existence/absence of MPI support in DFTB+ does not matter for the
#     # octopus tests
# fi
