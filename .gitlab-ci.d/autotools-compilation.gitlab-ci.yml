.autotools_configure:
  script:
    # Temporary directory
    - export TEMPDIRPATH=/var/tmp
    - export TMPDIR=/var/tmp

    # Directory for additional CI artifacts
    - mkdir _ci_artifacts
    - env -0 | sort -z | tr '\0' '\n' > _ci_artifacts/env.txt
    - module list 2> _ci_artifacts/loaded-modules.txt

    # mitigate known problems in configure
    - source .gitlab-ci.d/autotools-flags-for-problematic-dependencies.sh

    - echo -e "\e[0Ksection_start:`date +%s`:autoreconf[collapsed=true]\r\e[0KAutoreconf"
    - autoreconf -i
    - echo -e "\e[0Ksection_end:`date +%s`:autoreconf\r\e[0K"

    - mkdir _build
    - cd _build

    - echo -e "\e[0Ksection_start:`date +%s`:configure[collapsed=true]\r\e[0KConfigure call"
    - echo $CONFIGURE_FLAGS
    # eval is used to ensure correct quotes when expanding CONFIGURE_FLAGS;
    # without eval strings are split at all spaces despite having quotes around
    # them resulting in e.g. '--with-blas="-L$$MPSD_OPENBLAS_ROOT/lib"' '-lopenblas'
    - eval "../configure $CONFIGURE_FLAGS"
    - echo -e "\e[0Ksection_end:`date +%s`:configure\r\e[0K"

    # show missing librarys in config.h (but do not fail when libraries are missing)
    - $CI_PROJECT_DIR/scripts/autotools_check_config_h.py config.h config.log || true
  artifacts:
    when: always
    paths:
      - _build/config.log
      - _build/config.h
      - _build/compiler_warnings
      - _build/testsuite/runtime_warnings
      - _ci_artifacts/
  dependencies: []
  tags:
    - mpsd-sandybridge


.autotools_compile_warnings:
  extends: .autotools_configure
  stage: compiler_warnings
  variables:
    LOCAL_MAKEFLAGS: -j -s
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=8 --reservation=jacamar1 $SLURM_VERBOSITY"
  script:
    - !reference [.autotools_configure, script]

    - echo -e "\e[0Ksection_start:`date +%s`:make[collapsed=true]\r\e[0KCompile octopus"
    - make clean
    - set -o pipefail
    - make $LOCAL_MAKEFLAGS all 2>&1 | tee compiler_log
    - set +o pipefail
    - echo -e "\e[0Ksection_end:`date +%s`:make\r\e[0K"

    - ldd src/octopus | sort > "$CI_PROJECT_DIR/_ci_artifacts/octopus-${CI_JOB_NAME_SLUG}-ldd.txt"

    # mimic BuildBot's class WarningsCountingShellCommand
    - 'grep -Pe "(?i)^.*(warning|remark)[: ].*" compiler_log | tee compiler_warnings'

    # fail if the compiler_warnings file is not empty
    - "! test -s compiler_warnings"


.autotools_compile_test:
  extends: .autotools_configure
  stage: tests
  variables:
    LOCAL_MAKEFLAGS: -j 16
    # Number of Octopus jobs to run in parallel
    OCT_TEST_NJOBS: 16
    OMP_NUM_THREADS: 1
    TEST_TYPE: check
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=16 --time=3:00:00 --reservation=jacamar1 $SLURM_VERBOSITY"
    CHECK_REPORT: "${CI_JOB_NAME_SLUG}_${CI_COMMIT_SHA}"
  script:
    - !reference [.autotools_configure, script]

    - echo -e "\e[0Ksection_start:`date +%s`:make[collapsed=true]\r\e[0KCompile octopus"
    - make clean
    - make $LOCAL_MAKEFLAGS all
    - echo -e "\e[0Ksection_end:`date +%s`:make\r\e[0K"

    - ldd src/octopus | sort > "$CI_PROJECT_DIR/_ci_artifacts/octopus-${CI_JOB_NAME_SLUG}_ldd.txt"

    - echo -e "\e[0Ksection_start:`date +%s`:test[collapsed=true]\r\e[0KRun tests (make `echo $TEST_TYPE`)"
    - ulimit -s unlimited
    # on machines with inifiniband (currently not in the test pool) the following line might be required
    # - ulimit -l unlimited
    - ulimit -t 7200
    - make $TEST_TYPE
    - echo -e "\e[0Ksection_end:`date +%s`:test\r\e[0K"

  after_script:
    - CHECK_REPORT_FILE="_build/testsuite/${CHECK_REPORT}.yaml"
    - if [ -f $CHECK_REPORT_FILE ]; then curl -F "file=@$CHECK_REPORT_FILE" -F builder_name="${CI_JOB_NAME}" -F commit_hash="${CI_COMMIT_SHA}" -F token="${TESTSUITE_TOKEN}" https://www.octopus-code.org/testsuite/result/add; fi
    # - echo "codecov with $FAKE_CODECOV_AUTH"
