#[==============================================================================================[
#                                 Verrou compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindVerrou

Verrou compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET verrou::verrou)
	return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindVerrou)
find_path(VERROU_INCLUDE_DIR
	NAMES valgrind/verrou.h
	PATHS $ENV{VERROU_HOME}
	PATH_SUFFIXES include
	REQUIRED)
find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
	REQUIRED_VARS VERROU_INCLUDE_DIR
)

if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${VERROU_INCLUDE_DIR})
	add_library(verrou::verrou INTERFACE IMPORTED)
	target_include_directories(verrou::verrou INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(HAVE_VERROU 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
	URL https://github.com/edf-hpc/verrou
	DESCRIPTION "Verrou helps you look for floating-point round-off errors in programs"
	PURPOSE "Debugging instrumentation"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
