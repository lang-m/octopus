# _ci_intel:

NLopt is disabled because it causes problems during CMake configure:

The Intel compiler does not use the correct corresponding GCC (even though it is
on PATH). This results in a C++ stdlib mismatch (system GCC 10.2.1 vs toolchain
GCC 12.3.0). The Intel-GCC mapping should be done as part of the
toolchain/module setup.

External spglib is disabled in the preset because it is not yet available in the
Spack-based intel toolchains.
