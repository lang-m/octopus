#include <mpi.h>
#include <mpi-ext.h>
#if defined(HAVE_HIP) && (HAVE_HIP + 0)
#  if !(defined(MPIX_ROCM_AWARE_SUPPORT) && (MPIX_ROCM_AWARE_SUPPORT + 0))
#    error "ROCM-aware MPI is not available"
#  endif
#elif defined(HAVE_CUDA) && (HAVE_CUDA + 0)
#  if !(defined(MPIX_CUDA_AWARE_SUPPORT) && (MPIX_CUDA_AWARE_SUPPORT + 0))
#    error "CUDA-aware MPI is not available"
#  endif
#else
#  error "GPU-aware MPI is not available"
#endif
int main() {}
