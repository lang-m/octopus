#[==============================================================================================[
#                                   GD compatibility wrapper                                   #
]==============================================================================================]

#[===[.md
# FindGD

GDLib compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET GD::GD)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindGD)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES GD
        PKG_MODULE_NAMES gdlib)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(GD::GD ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_GDLIB 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/libgd/libgd
        DESCRIPTION "GD is an open source code library for the dynamic creation of images by programmers"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
