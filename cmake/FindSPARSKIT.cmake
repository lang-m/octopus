#[==============================================================================================[
#                                SPARSKIT compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# FindSPARSKIT

SPARSKIT compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET SPARSKIT::sparskit)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindSPARSKIT)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES SPARSKIT sparskit
        PKG_MODULE_NAMES sparskit)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(SPARSKIT::sparskit ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(SPARSKIT_LIBRARY
            NAMES skit
    )
    mark_as_advanced(SPARSKIT_LIBRARY)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS SPARSKIT_LIBRARY
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS)
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${SPARSKIT_LIBRARY})
        add_library(SPARSKIT::sparskit UNKNOWN IMPORTED)
        set_target_properties(SPARSKIT::sparskit PROPERTIES
                IMPORTED_LOCATION ${SPARSKIT_LIBRARY}
                IMPORTED_LINK_INTERFACE_LANGUAGES Fortran
        )
        target_link_libraries(SPARSKIT::sparskit INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_SPARSKIT 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/efocht/SPARSKIT2
        DESCRIPTION "The SPARSKIT2 collection by Y. Saad, with a few bug fixes and basic port to SX-Aurora VE. Not tuned."
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
